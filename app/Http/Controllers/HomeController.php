<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('profile');
    }

    public function editprofile(Request $request, $id)
    {
        $user =  User::findOrfail($id);
         $data = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'job' => 'required',
             'date' => ['required', 'date','before:2000-01-1' ,'max:255'],
             'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
             'password_confirmation' => 'min:6',
             'file' => 'required',

        ]);
        $data['password'] = bcrypt($data['password']);
        $user->update($data);

        if($request->file) {
            $fileName = time() . '_' . $request->file->getClientOriginalName();

            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');



            $user->file = 'uploads/'.$fileName;

            $user->save();
        }




        return back()
            ->with('success','User Created Successfully');
    }
}
