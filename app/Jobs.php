<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table= 'jobs';
    protected $fillable = [
        'title', 'qualification', 'campany','gov','address', 'contract', 'details'
    ];
}
