<?php

use App\Jobs;
use Illuminate\Database\Seeder;

class JobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Jobs::create([
            'title' => 'Web developer',
            'qualification' => 'computer science',
            'campany' => 'شركة دلتا',
            'gov' => 'مصر',
            'address' => 'المنصورة',
            'contract' => '',
            'details' => 'التقديم حتى 12 ابريل اعلان وظائف مصلحة الخبراء بوزارة العدل البكالريوس',
        ]);
        Jobs::create([
            'title' => 'ios Developer',
            'qualification' => 'computer science',
            'campany' => 'شركة دلتا',
            'gov' => 'مصر',
            'address' => 'المنصورة',
            'contract' => '',
            'details' => 'التقديم حتى 12 ابريل اعلان وظائف مصلحة الخبراء بوزارة العدل البكالريوس',
        ]);
        Jobs::create([
            'title' => 'Technical supportr',
            'qualification' => 'computer science',
            'campany' => 'شركة دلتا',
            'gov' => 'مصر',
            'address' => 'المنصورة',
            'contract' => '',
            'details' => 'التقديم حتى 12 ابريل اعلان وظائف مصلحة الخبراء بوزارة العدل البكالريوس',
        ]);
        Jobs::create([
            'title' => 'Project manager',
            'qualification' => 'computer science',
            'campany' => 'شركة دلتا',
            'gov' => 'مصر',
            'address' => 'المنصورة',
            'contract' => '',
            'details' => 'التقديم حتى 12 ابريل اعلان وظائف مصلحة الخبراء بوزارة العدل البكالريوس',
        ]);
        Jobs::create([
            'title' => 'it',
            'qualification' => 'computer science',
            'campany' => 'شركة دلتا',
            'gov' => 'مصر',
            'address' => 'المنصورة',
            'contract' => '',
            'details' => 'التقديم حتى 12 ابريل اعلان وظائف مصلحة الخبراء بوزارة العدل البكالريوس',
        ]);
    }
}
