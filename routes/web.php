<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.pages.home');
});

Route::get('/new-register', function () {
    return view('front.pages.register');
});

Route::get('/new-login', function () {
    return view('front.pages.login');
});


Route::get('/profile', function () {
    return view('front.pages.profile');
})->name('profile');

Route::get('/edit_profile', function () {
    return view('front.pages.edit_profile');
});

Route::get('/contact', function () {
    return view('front.pages.contact');
});
Route::get('/contact','ContactController@index');
Route::post('/contact','ContactController@store');
Route::post('/search', 'ContactController@search')->name('search');
Route::get('/jobs','jobsController@index');
Route::post('/post_profile/{id}','HomeController@editprofile');
Route::get('/team', function () {
    return view('front.pages.team');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

