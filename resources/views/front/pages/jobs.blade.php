@extends('front.layouts.index')

@section('content')
    @push('css')
        <style>
            a{
                text-decoration: none;
            }
        </style>
    @endpush

    <section class="wrapper">


            <div class="content">
                <div class="container">
                    <div class="row">
                        @foreach($jobs as $one)
                        <div class="col-sm-3">
                            <div class="card">
                                <a class="img-card" href="#">
                                    <img src="{{url('/')}}/front/img/jobs.jpg" />
                                </a>
                                <div class="card-content">
                                    <h4 class="card-title">
     الوظيفة :                                  {{$one->title}}

                                    </h4>
                                    <h4 class="" style="color: #000">
                                      شركة : {{$one->campany}}
                                        <br>
                                       العنوان : {{$one->address}}
                                    </h4>

                                    <p class="">
                                        {{$one->details}}
                                    </p>
                                </div>
                                <div class="card-read-more">
                                    <a style="text-decoration: none" class="btn  btn-danger" href="{{url('new-login')}}" class="btn btn-link btn-block">
                                        سجل معنا
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

    </section>




@endsection
