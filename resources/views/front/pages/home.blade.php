@extends('front.layouts.index')
@section('content')

<div class="aboutme">
    <div class="container">

        <div class="image">
            <img src="{{url('/')}}/front/home/img/img01.png" style="width:400px;height: 400px;" alt="test">
        </div>

        <div class="info">
            <h2>هل تبحث عن وظيفة ؟ </h2>
            <p>وظائفنا  ستساعدك في الوصول الي افضل الفرص الوظيفية في المكان المناسب لك. اكتب مدينك واختر القسم واستعرض الوظائف المتاحة وتقدم للوظيفة
            </p>

            <div class="hobbies">


                <form action="search" method="post">
                    @csrf
                    <!--start about box content-->
                    <div class="content">
                        <!-- start icon div-->
                        <select name="gov" style="width:200px;height:50px;margin-left:80px ;">

                            <option name="" disabled selected>اختر المحافظة </option>
                            <option name="الدقهلية">الدقهلية</option>
                            <option name="البحيرة">البحيرة</option>
                            <option name="الشرقية">الشرقية</option>
                            <option name="القليوبية">القليوبية</option>
                            <option name="الجيزة">الجيزة</option>
                            <option name="الغربية">الغربية</option>
                            <option name="الإسكندرية">الإسكندرية</option>
                            <option name="أسوان">أسوان</option>
                            <option name=" بني سويف"> بني سويف</option>
                            <option name="القاهرة">القاهرة</option>
                            <option name="دمياط" >دمياط</option>
                            <option name="االفيوم">االفيوم</option>
                            <option name="الإسماعيلية">الإسماعيلية</option>
                            <option name="اكفر الشيخ">اكفر الشيخ</option>
                            <option name="الأقصر">الأقصر</option>
                            <option name="مطروح">مطروح</option>
                            <option name="المنيا">المنيا</option>
                            <option name="المنوفية">المنوفية</option>
                            <option name="الوادي الجديد">الوادي الجديد</option>
                            <option name="شمال سيناء">شمال سيناء</option>
                            <option name="البحر الأحمر">البحر الأحمر</option>
                            <option name="جنوب سيناء">جنوب سيناء</option>
                        </select>

                        <select name="jop" style="width:200px;height:50px;margin-left: 20px;">
                            <option name="" disabled selected>من فضلك اختر الوظيفة </option>
                            @foreach($jobs as $one)
                                <option value="{{$one->title}}">{{$one->title}}</option>
                            @endforeach
                        </select>

                        <input type="submit" value="ابحث" name="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end icon div-->


<!--end about me-->

<!--  start features -->
<div class="features text-center">
    <div class="container">
        <h2 class="text-center" style="margin-bottom:50px ;font-weight: bold;color: #fff;">الوظائف المتاحة فى المحافظات</h2>
        <div class="row">

            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px;">

                <h3>الدقهلية </h3>
                <p>200 وظيفة متاحة</p>

            </div>

            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>الشرقية </h3>
                <p>  250 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>الغربية </h3>
                <p>412 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>الجيزة </h3>
                <p>235 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>البحيرة </h3>
                <p>214 وظيفة متاحة</p>

            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>الإسكندرية </h3>
                <p>125 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>كفر الشيخ </h3>
                <p>147 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>الدقهلية </h3>
                <p>132 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>المنوفية </h3>
                <p>124 وظيفة متاحة</p>

            </div>
            <div class="col-sm-6 col-lg-2" style="background-color: #fff;height: 100px;width: 50px;margin-right: 10px">

                <h3>دمياط </h3>
                <p>215 وظيفة متاحة</p>

            </div>
        </div>
    </div>
</div>
<!--  end features -->

<!-- start latest-post-->
<div class="latest-post bg-light ">
    <div class="container">
        <h2 class="text-center">المقالات </h2>
        <p class="paragrph text-center">المقالات ادناة ستساعدك في تنمية مهاراتك</p>


        <div class="row">
            <!-- start grid column-->
            <div class="col-md-6 col-lg-4">
                <div class="card">
                    <img src="{{url('/')}}/front/home/img/Email-skills.jpg" alt="photo" class="card-img-top">
                    <div class="card-body text-left">
                        <h5 class="card-subtitle mb-2 text-muted">نصائح لكتابة بريد إلكترونى أفضل</h5>
                        <p class="card-text">email communication is a very important skill..</p>
                        <a href="#" class="card-link">Read More</a>
                    </div>
                </div>
            </div>
            <!-- end grid column-->
            <!-- start grid column-->
            <div class="col-md-6 col-lg-4">
                <div class="card">
                    <img src="{{url('/')}}/front/home/img/نصائح-للمبتدذين.jpg" alt="photo" class="card-img-top">
                    <div class="card-body text-left">

                        <h5 class="card-subtitle mb-2 text-muted">نصائح للمبتدئين في مجال ال Software Engineering</h5>
                        <p class="card-text">كام نصيحة صغيرة للناس الي بيبتدوا حياتهم.</p>
                        <a href="#" class="card-link">Read More</a>
                    </div>
                </div>
            </div>
            <!-- end grid column-->
            <!-- start grid column-->
            <div class="col-md-6 col-lg-4">
                <div class="card">
                    <img src="{{url('/')}}/front/home/img/Screen-Shot-2021-01-25-at-10.55.34-AM.png" alt="photo" class="card-img-top">
                    <div class="card-body text-left">

                        <h5 class="card-subtitle mb-2 text-muted">Create chat app using react-native and Firebase</h5>
                        <p class="card-text"></p>
                        <a href="#" class="card-link">Read More</a>
                    </div>
                </div>
            </div>
            <!-- end grid column-->
        </div>
    </div>
</div>


<section class="tour-package">
    <div class="container">

        <div class="package-carousel owl-carousel">
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">
                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">
                            <h5 class="name">اعلان عن وظيفة منسق خدمة عملاء بالقاهرة</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">

                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">
                            <h5 class="duration">اعلان عن وظيفة مسؤول تنفيذ بالدقهلية</h5>

                        </div>

                    </div>
                </div>
            </div>
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">
                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">
                            <h5 class="duration">الوظيفة: مساعد مدير مطعم - التجمع الخامس - القاهرة</h5>

                        </div>

                    </div>
                </div>
            </div>
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">
                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">

                            <h5 class="duration">الوظيفة: مراقب وقت (تايم كيبر) (صيانة) - العامرية - الاسكندرية</h5>
                        </div>

                    </div>
                </div>
            </div>
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">
                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">
                            <h5 class="duration">الوظيفة: رئيس قسم الشئون الإدارية - القاهرة الجديدة - القاهرة</h5>

                        </div>

                    </div>
                </div>
            </div>
            <div class="single-package">
                <div class="img">
                    <img src="{{url('/')}}/front/home/img/504b542fc751669514fc1d108e467265.jpg" alt="">
                </div>
                <div class="content">
                    <div class="name-price">
                        <div class="np-name">
                            <h5 class="duration">مدير مطعم و كافية ( الجيزة و القاهرة )</h5>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@endsection
@push('css')
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/jquery-ui.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/animate.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/stellarnav.min.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/magnific-popup.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/style.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/homepage.css">
    <link rel="stylesheet" href="{{url('/')}}/front/home/css/responsive.css">

@endpush
@push('js')
    <script src="{{url('/')}}/front/home/js/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery-ui.js"></script>
    <script src="{{url('/')}}/front/home/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery.counterup.min.js"></script>
    <script src="{{url('/')}}/front/home/js/countdown.js"></script>
    <script src="{{url('/')}}/front/home/js/stellarnav.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery.fancybox.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery.scrollUp.js"></script>
    <script src="{{url('/')}}/front/home/js/wow.min.js"></script>
    <script src="{{url('/')}}/front/home/js/jquery.waypoints.min.js"></script>
    <script src="{{url('/')}}/front/home/js/popper.min.js"></script>
    <script src="{{url('/')}}/front/home/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/front/home/js/theme.js"></script>
@endpush
