@extends('front.layouts.index')

@section('content')
    @push('css')
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/bootstrap.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/style.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/contact.css">


    @endpush
    @push('js')


        <script src="{{url('/')}}/front/home/js/popper.min.js"></script>
        <script src="{{url('/')}}/front/home/js/jquery-3.5.1.min.js"></script>
        <script src="{{url('/')}}/front/home/js/bootstrap.js"></script>
        <script src="{{url('/')}}/front/home/js/main.js"></script>
        @endpush



    @if(Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ \Illuminate\Support\Facades\Session::get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    @endif
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">اتصل بنا</h2>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-4">
                    <div class="info">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس</p>
                        </div>

                        <div>
                            <i class="fa fa-envelope"></i>
                            <p>info@example.com</p>
                        </div>

                        <div>
                            <i class="fa fa-phone"></i>
                            <p>+1 5589 55488 55s</p>
                        </div>

                    </div>
                </div>

                <div class="col-lg-5 col-md-8">
                    <div class="form">
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>
                        <form action="contact" method="post" role="form" class="contactForm">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="الاسم" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="البريد الالكتروني" data-rule="email" data-msg="Please enter a valid email" />
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="الموضوع" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                @if ($errors->has('subject'))
                                    <span class="text-danger">{{ $errors->first('subject') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="الرساله"></textarea>
                                @if ($errors->has('message'))
                                    <span class="text-danger">{{ $errors->first('message') }}</span>
                                @endif
                            </div>
                            <div class="text-center"><button type="submit" name="submit">ارسال</button></div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection
