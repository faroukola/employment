@extends('front.layouts.index')

@section('content')
    @push('css')

        <link rel="stylesheet" href="{{url('/')}}/front/home/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/fontawesome-free-5.15.3-web/css/all.min.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/style.css">
        <link rel="stylesheet" href="{{url('/')}}/front/home/css/Team.css">
    @endpush
    @push('js')

        <script src="{{url('/')}}/front/home/js/popper.min.js"></script>
        <script src="{{url('/')}}/front/home/js/jquery-3.5.1.min.js"></script>
        <script src="{{url('/')}}/front/home/js/bootstrap.min.js"></script>
        <script src="{{url('/')}}/front/home/js/main.js"></script>
    @endpush

    <div id="team" class="section wb">
        <div class="container">
            <div class="section-title text-center">
                <h3>فريق العمل</h3>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/1.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">محمد عبدالرحمن سعد</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/2.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">محمد احمد</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/3.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">طارق علاء</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/4-.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">نورالدين محمد</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/5.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">مجدي محمد</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/6.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">فاروق احمد</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/7.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">محمد عبدالرحمن العزب</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/8.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">محمد عادل</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6"></div>
                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/9.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">محمد متولي</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="our-team">
                        <div class="pic">
                            <img src="{{url('/')}}/front/home/img/10.jpg" alt="">
                        </div>
                        <div class="team-content">
                            <h3 class="title">عمر مجدي</h3>

                            <ul class="social">
                                <li><a href="#" class="fab fa-facebook-f"></a></li>
                                <li><a href="#" class="fab fa-twitter"></a></li>
                                <li><a href="#" class="fab fa-google-plus-g"></a></li>
                                <li><a href="#" class="fab fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6"></div>

            </div>
        </div>
    </div>


    </body>
    </html>





@endsection
