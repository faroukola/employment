@extends('front.layouts.index')

@section('content')
    @push('css')
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/feather/feather.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/ti-icons/css/themify-icons.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/select2/select2.min.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/css/vertical-layout-light/style.css">
    @endpush
    @push('js')
        <script src="{{url('/')}}/front/vendors/js/vendor.bundle.base.js"></script>
        <!-- endinject -->
        <!-- Plugin js for this page -->
        <script src="{{url('/')}}/front/vendors/typeahead.js/typeahead.bundle.min.js"></script>
        <script src="{{url('/')}}/front/vendors/select2/select2.min.js"></script>
        <!-- End plugin js for this page -->
        <!-- inject:js -->
        <!-- endinject -->
        <!-- Custom js for this page-->
        @endpush
        </head>

        <body class="text-center">

        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">


            <div class="container-scroller">

                <div class="container-fluid page-body-wrapper">

                    <!-- partial:../../partials/_sidebar.html -->

                    @include('front.includes.menu')
                    <!-- partial -->
                    <div class="main-panel">
                        <div class="content-wrapper">
                            <div class="row">
                                <div class="col-md-12 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">بيانات البروفايل</h4>

                                            <form class="forms-sample">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputUsername1">Username</label>
                                                        <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Username" value="{{ Auth::user()->name }}" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Email address</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" disabled value="{{ Auth::user()->email }}" >
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputPassword1">Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" disabled value="{{ Auth::user()->password }}">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputPassword1">jop</label>
                                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" disabled value="{{ Auth::user()->job }}">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputPassword1">type</label>
                                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" disabled value="@if(Auth::user()->type == 0 ) Individuals @else company @endif">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputPassword1">date</label>
                                                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" disabled value="{{Auth::user()->date}}">
                                                    </div>

                                                </div>


                                                <a href="{{url('edit_profile')}}" class="btn btn-primary mr-2">edit_profile</a>

                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- content-wrapper ends -->
                            <!-- partial:../../partials/_footer.html -->

                            <!-- partial -->
                        </div>
                        <!-- main-panel ends -->
                    </div>
                    <!-- page-body-wrapper ends -->
                </div>
@endsection
