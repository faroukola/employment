@extends('front.layouts.index')
@section('content')
    <div class="container"  style="background-image: url({{asset('/front/img/register.jpg')}}); background-size: cover">
        <div class="row justify-content-center ">
            <div class="col-md-12">



            </div>

        </div>
    </div>

    <div id="register" class="container mt-5 pt-5">
        <div class="card mx-auto border-0">
            <div class="card-header border-bottom-0 bg-transparent">
                <ul class="nav nav-tabs justify-content-center pt-4" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active text-primary" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login"
                           aria-selected="true">الشركات</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-primary" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register"
                           aria-selected="false">الافراد</a>
                    </li>
                </ul>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ \Illuminate\Support\Facades\Session::get('success') }}
                    @php
                        \Illuminate\Support\Facades\Session::forget('success');
                    @endphp
                </div>
            @endif
            <div class="card-body pb-4">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf


                            <div class="form-group">
                                <label for="exampleInputname">Name</label>
                                <input type="text" class="form-control" id="exampleInputname"  name="name"  placeholder="Enter name">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputjob">job</label>

                                <select class="form-control"   name="job" id="exampleInputjob" >
                                    @foreach($jobs as $one)
                                    <option value="{{$one->title}}">{{$one->title}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('job'))
                                    <span class="text-danger">{{ $errors->first('job') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputDate">birthday</label>
                                <input type="date" class="form-control" id="exampleInputDate" name="date" placeholder="Date">

                                @if ($errors->has('date'))
                                    <span class="text-danger">{{ $errors->first('date') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail">Email</label>
                                <input type="Email" class="form-control" id="exampleInputEmail" name="email" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif

                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type"  id="gridRadios1" value="1" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                female
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"  name="type" id="gridRadios2" value="0">
                                            <label class="form-check-label" for="gridRadios2">
                                                male
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Location</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option name="qena" >قنا</option>
                                    <option name="mansoura" >المنصورة</option>
                                    <option name="tanta">طنطا</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3">Password</label>

                                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">password_confirmation</label>
                                <input type="password" class="form-control"  name="password_confirmation" id="exampleInputPassword1" placeholder="password_confirmation"  value="" >

                            </div>
                            <input type="hidden" class="form-control" value="1" name="CompanyOrPersons" >
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf


                            <div class="form-group">
                                <label for="exampleInputname">Name</label>
                                <input type="text" class="form-control" id="exampleInputname"  name="name"  placeholder="Enter name">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputjob">job</label>

                                <select class="form-control"   name="job" id="exampleInputjob" >
                                  
                                    @foreach($jobs as $one)
                                        <option value="{{$one->title}}">{{$one->title}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('job'))
                                    <span class="text-danger">{{ $errors->first('job') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputDate">birthday </label>
                                <input type="date" class="form-control" id="exampleInputDate" name="date" placeholder="Date">
                                @if ($errors->has('date'))
                                    <span class="text-danger">{{ $errors->first('date') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail">Email</label>
                                <input type="Email" class="form-control" id="exampleInputEmail" name="email" placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Type</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="type"  id="gridRadios1" value="1" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                female
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"  name="type" id="gridRadios2" value="0">
                                            <label class="form-check-label" for="gridRadios2">
                                                male
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Location</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option name="qena" >قنا</option>
                                    <option name="mansoura" >المنصورة</option>
                                    <option name="tanta">طنطا</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword3">Password</label>

                                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">password_confirmation</label>
                                <input type="password" class="form-control"  name="password_confirmation" id="exampleInputPassword1" placeholder="password_confirmation"  value="" >

                            </div>

                            <div class="form-group">

                                <input type="hidden" class="form-control" value="0" name="CompanyOrPersons" >
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('css')
<style>
    body {
        background: url('https://images.unsplash.com/photo-1534408679207-69b9615e55a7?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=cfbabd80cd2d5cae495a2a732d473562') no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .card {
        width: 40%;
    }
</style>
@endpush
