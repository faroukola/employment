@extends('front.layouts.index')

@section('content')
    @push('css')
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/feather/feather.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/ti-icons/css/themify-icons.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/css/vendor.bundle.base.css">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/select2/select2.min.css">
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{url('/')}}/front/vendors/css/vertical-layout-light/style.css">
    @endpush
    @push('js')

        <script src="{{url('/')}}/front/vendors/js/vendor.bundle.base.js"></script>
        <!-- endinject -->
        <!-- Plugin js for this page -->
        <script src="{{url('/')}}/front/vendors/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{url('/')}}/front/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
        <!-- End plugin js for this page -->
        <!-- inject:js -->
        <script src="{{url('/')}}/front/vendors/js/off-canvas.js"></script>
        <script src="{{url('/')}}/front/vendors/js/hoverable-collapse.js"></script>
        <script src="{{url('/')}}/front/vendors/js/template.js"></script>
        <script src="{{url('/')}}/front/vendors/js/settings.js"></script>
        <script src="{{url('/')}}/front/vendors/js/todolist.js"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="{{url('/')}}/front/vendors/js/form-validation.js"></script>
        <script src="{{url('/')}}/front/vendors/js/bt-maxLength.js"></script>
        <!-- End custom js for this page-->
        @endpush
        </head>

        <body class="text-center">

        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">




            <div class="container-scroller">
                <!-- partial:../../partials/_navbar.html -->

                <!-- partial -->
                <div class="container-fluid page-body-wrapper">

                    <!-- partial:../../partials/_sidebar.html -->
                @include('front.includes.menu')
                    <!-- partial -->
                    <div class="main-panel">
                        <div class="content-wrapper">
                            <div class="row">

                                    <div class="col-lg-12">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success">
                                                {{ \Illuminate\Support\Facades\Session::get('success') }}
                                                @php
                                                    \Illuminate\Support\Facades\Session::forget('success');
                                                @endphp
                                            </div>
                                        @endif
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">تعديل البروفايل</h4>


                                                <form class="forms-sample" id="" method="post" action='{{url('post_profile/'.Auth::user()->id )}}'  enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputUsername1">Username</label>
                                                            <input type="text" class="form-control" name="name" id="exampleInputUsername1" placeholder="Username" value="{{ Auth::user()->name }}" required>
                                                            @if ($errors->has('name'))
                                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputEmail1">Email address</label>
                                                            <input type="email" class="form-control" name="email"  id="exampleInputEmail1" placeholder="Email"  value="{{ Auth::user()->email }}"  required>
                                                            @if ($errors->has('email'))
                                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputPassword1">Password</label>
                                                            <input type="password" class="form-control"  name="password" id="exampleInputPassword1" placeholder="Password"  value="{{ Auth::user()->password }}" required>
                                                            @if ($errors->has('password'))
                                                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputPassword1">password_confirmation</label>
                                                            <input type="password" class="form-control"  name="password_confirmation" id="exampleInputPassword1" placeholder="password_confirmation"  value="{{ Auth::user()->password_confirmation }}" required>
                                                            @if ($errors->has('password_confirmation'))
                                                                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                                            @endif
                                                        </div>


                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputjob">job</label>

                                                            <select class="form-control"   name="job" id="exampleInputjob" >
                                                                <option value="programmeer">programmeer</option>
                                                                <option value="programmeer">programmeer</option>
                                                                <option value="programmeer">programmeer</option>
                                                                <option value="programmeer">programmeer</option>
                                                            </select>
                                                            @if ($errors->has('job'))
                                                                <span class="text-danger">{{ $errors->first('job') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputPassword1">date</label>
                                                            <input type="date" class="form-control" id="exampleInputPassword1" placeholder="date"  name="date" value="{{Auth::user()->date}}">

                                                            @if ($errors->has('date'))
                                                                <span class="text-danger">{{ $errors->first('date') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputPassword1">file</label>
                                                            <input type="file" class="form-control" id="exampleInputPassword1" placeholder="file"  name="file">

                                                            @if ($errors->has('file'))
                                                                <span class="text-danger">{{ $errors->first('file') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>


{{--                                                    <a href="{{ asset('public/' .Auth::user()->file) }}">Open the pdf!</a>--}}




                                                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                                    <button class="btn btn-light">Cancel</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>



                            </div>

                        </div>

                    </div>

                </div>
@endsection
