<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>



            <title>my website</title>

            <link rel="stylesheet" href="{{url('/')}}/front/css/bootstrap.css">
            <link rel="stylesheet" href="{{url('/')}}/front/css/font-awesome.min.css">
            <link rel="stylesheet" href="{{url('/')}}/front/css/style.css">
            <link rel="stylesheet" href="{{url('/')}}/front/css/newlogin.css">
{{--            <link rel="stylesheet" href="{{url('/')}}/front/css/create-account.css">--}}


             </head>

            <body>
           <!-- start upperbar-->
           <div class="upper-bar bg-secondary">
            <div class="container">

            </div>
           </div>

           <!-- end upperbar-->

           <!-- start navbar-->
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

           <div class="container">

            <!--logo-->
            <a class="navbar-brand" href="">
              <span>وظائف فى مصر</span>

             </a>

            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="{{url('contact')}}"> اتصل بنا</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('team')}}"> فريق العمل</a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{url('jobs')}}"> كل الوظائف   </a>
                          @guest
                              @if (Route::has('register'))


                    </li>
                       <li class="nav-item">
                        <a class="nav-link" href="{{url('new-register')}}">  مسخدم جديد</a>
                    </li>
                    @endif


                    <li class="nav-item">
                      <a class="nav-link" href="{{url('new-login')}}">  تسجيل الدخول</a>
                  </li>

                    @else


                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a   class="dropdown-item"  href="{{ route('profile') }}">

                                    {{ __('Home') }}
                                </a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>


                    @endguest
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">  الصفحة الرئيسية</a>
                </li>
                <li>

                </li>
                  </li>
                              </div>
                              </li>
                            </ul>
                        </div>
                       </div>

                      </nav>

