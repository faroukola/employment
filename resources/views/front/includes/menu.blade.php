<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">

        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
                <i class="icon-columns menu-icon"></i>
                <span class="menu-title">البروفيل</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{url('/profile')}}">  بيانات{{ Auth::user()->name }}  </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('/edit_profile')}}">تعديل   {{ Auth::user()->name }}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">الوظائف</a></li>
                    <li class="nav-item"><a class="nav-link" href="">الخروج</a></li>
                </ul>
            </div>
        </li>

    </ul>
</nav>
