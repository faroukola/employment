


             <script src="{{url('/')}}/front/js/popper.min.js"></script>
             <script src="{{url('/')}}/front/js/jquery-3.5.1.min.js"></script>
             <script src="{{url('/')}}/front/js/bootstrap.js"></script>
             <script src="{{url('/')}}/front/js/main.js"></script>
             @stack('css')
             @stack('js')
            </body>

    </html>
