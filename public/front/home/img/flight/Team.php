<html>

    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title>Home</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/stellarnav.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/Team.css">
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/png" href="img/IMG_23052021_104240_(116_x_67_pixel).jpg">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>

    <body>
        <div id="preloader"></div>
        <header id="_header">
            <div class="header-top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-8 col-12">
                            <div class="hta-contact">
                                <a href="tel:01069663900" class="text"><span class="icon"><i
                                            class="fas fa-phone"></i></span>(+880) 123456789</a>
                                <a href="mailto:elshadid_mohamed@yahoo.com" class="text"><span class="icon"><i
                                            class="fas fa-envelope"></i></span>elshadid_mohamed@yahoo.com</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-12" style="margin-left: 30%"> 
                            <ul class="hta-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="header-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-12">
                            <div class="logo">
                                <a href="index.php" class="link"><img src="img/IMG_23052021_104240_(116_x_67_pixel).jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-6 col-6">
                            <div class="main-menu stellarnav">
                                <ul>
                                    <li><a href="index.php">الرئيسية</a> </li>
                                    <li><a href="Show.php">عرض الرحلات</a></li>
                                    <li><a href="login.php">تسجل الدخول</a></li>
                                    <li><a href="create_account.php">التسجيل في الموقع</a></li>
                                    <li><a href="#">فريق العمل</a></li>
                                    <li><a href="Contact.php">اتصل بنا</a></li>
                                   
                                  
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-2 col-6">
                            <div class="search">
                                <button type="button" class="search-btn btn" data-toggle="modal"
                                    data-target="#exampleModalLong"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </header>
        
        <div id="team" class="section wb">
		<div class="container">
			<div class="section-title text-center">
				<h3>فريق العمل</h3>
			</div>
		
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
                                                    <img src="img/1.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">الحسن مجاهد</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
                                                    <img src="img/2.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">محمد علاء</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
                                                    <img src="img/3.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">احمد عاطف</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
                                                    <img src="img/4.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">نادر السيد</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
                           
                            <div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
							<img src="img/5.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">عمر خالد</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<div class="pic">
							<img src="img/6.jpg" alt="">
						</div>
						<div class="team-content">
							<h3 class="title">محمد مصطفي</h3>
							
							<ul class="social">
								<li><a href="#" class="fab fa-facebook-f"></a></li>
								<li><a href="#" class="fab fa-twitter"></a></li>
								<li><a href="#" class="fab fa-google-plus-g"></a></li>
								<li><a href="#" class="fab fa-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				
				
				
                           
                            
                           
                           
		</div>
	</div>
        
        
          <script>
            function lettersOnly(input) {
                var regex = /[^0-9]/gi;
                input.value = input.value.replace(regex, "");
            }
        </script>
        <!-- restricated-input-text -->
        <!-- Scripts -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/stellarnav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>
        <script src="js/jquery.scrollUp.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/theme.js"></script>
        
    </body>
</html>
