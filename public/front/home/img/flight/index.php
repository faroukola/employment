<!DOCTYPE html>
<html>

    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title>Home</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/stellarnav.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/png" href="img/IMG_23052021_104240_(116_x_67_pixel).jpg">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>

    <body>
        <div id="preloader"></div>
        <header id="_header">
            <div class="header-top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-8 col-12">
                            <div class="hta-contact">
                                <a href="tel:01069663900" class="text"><span class="icon"><i
                                            class="fas fa-phone"></i></span>(+880) 123456789</a>
                                <a href="mailto:elshadid_mohamed@yahoo.com" class="text"><span class="icon"><i
                                            class="fas fa-envelope"></i></span>elshadid_mohamed@yahoo.com</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-12" style="margin-left: 30%"> 
                            <ul class="hta-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="header-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-12">
                            <div class="logo">
                                <a href="index.php" class="link"><img src="img/IMG_23052021_104240_(116_x_67_pixel).jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-6 col-6">
                            <div class="main-menu stellarnav">
                                <ul>
                                    <li><a href="#">الرئيسية</a> </li>
                                    <li><a href="Show.php">عرض الرحلات</a></li>
                                    <li><a href="login.php">تسجل الدخول</a></li>
                                    <li><a href="create_account.php">التسجيل في الموقع</a></li>
                                    <li><a href="Team.php">فريق العمل</a></li>
                                    <li><a href="Contact.php">اتصل بنا</a></li>
                                   
                                  
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-2 col-6">
                            <div class="search">
                                <button type="button" class="search-btn btn" data-toggle="modal"
                                    data-target="#exampleModalLong"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </header>
        <section class="hero-area" id="_hero" style="background-image: url('img/plane.jpg');background-size: cover;width: 100%;height: 500px">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-12">
                        <div class="hero-content">
                            <h2 class="title wow fadeInUp animated" data-wow-duration="0.3s" data-wow-delay="0.3s">احجز تذاكره طيران</h2>
                         
                        </div>
                    </div>
                </div>
            </div>
        </section>
      
        <section class="why-area">
            <div class="container">
                
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 col-12 wow fadeIn animated" data-wow-duration="0.3s"
                        data-wow-delay="0.5s">
                        <div class="single-why">
                            <div class="icon">
                                <span class="icon-box">
                                    <img src="img/home1/why-icon-1.png" alt="">
                                </span>
                            </div>
                            <div class="content">
                                <h4 class="name">تحديد مواعيد السفر</h4>
                             
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 wow fadeIn animated" data-wow-duration="0.3s"
                        data-wow-delay="0.6s">
                        <div class="single-why">
                            <div class="icon">
                                <span class="icon-box">
                                    <img src="img/home1/why-icon-2.png" alt="">
                                </span>
                            </div>
                            <div class="content">
                                <h4 class="name">تحديد الرحلة</h4>
                              
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12 wow fadeIn animated" data-wow-duration="0.3s"
                        data-wow-delay="0.7s">
                        <div class="single-why">
                            <div class="icon">
                                <span class="icon-box">
                                    <img src="img/home1/why-icon-3.png" alt="">
                                </span>
                            </div>
                            <div class="content">
                                <h4 class="name">تحديد مكان السفر</h4>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      
        
        
       
      
        <footer>
           
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="fba-content">
                        <p class="copyright">جميع الحقوق محفوظة &copy;2021 </p>
                        <a href="#_header" class="go-top smoothscroll"><img src="img/home1/top.png" alt=""></a>
                    </div>
                </div>
            </div>
        </footer>
        <div class="search-modal modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="#" method="POST" class="xs-search-group">
                        <input type="search" class="form-control" name="search" id="search" placeholder="Search">
                        <button type="submit" class="search-button"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
            <button class="close-modal" type="button" data-dismiss="modal">x</button>
        </div>
        <!-- restricated-input-text -->
        <script>
            function lettersOnly(input) {
                var regex = /[^0-9]/gi;
                input.value = input.value.replace(regex, "");
            }
        </script>
        <!-- restricated-input-text -->
        <!-- Scripts -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/stellarnav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>
        <script src="js/jquery.scrollUp.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/theme.js"></script>
    </body>

</html>