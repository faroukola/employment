// -----------------------------

//   js index
/* =================== */
/*  
    

    

*/
// -----------------------------


(function($) {
    "use strict";

    /*---------------------
    preloader
    --------------------- */
    $(window).on('load', function() {
        $('#preloader').fadeOut('slow', function() { $(this).remove(); });
    });

    /*---------------------
    wow
    --------------------- */
    new WOW().init();

    /*----------------------------
     stellarnav
    ------------------------------ */
    jQuery('.stellarnav').stellarNav({
        theme: 'light',
        breakpoint: 960,
        position: 'right',
        phoneBtn: '18009997788',
        locationBtn: 'https://www.google.com/maps'
    });

    /*-----------------
    sticky
    -----------------*/
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 190) {
            $('.header-bottom-area').addClass('navbar-fixed-top');
        } else {
            $('.header-bottom-area').removeClass('navbar-fixed-top');
        }
    });

    /*-----------------
    scroll-up
    -----------------*/
    $.scrollUp({
        scrollText: '<i class="fa fa-arrow-up" aria-hidden="true"></i>',
        easingType: 'linear',
        scrollSpeed: 1500,
        animation: 'fade'
    });

    /*------------------------------
    counter
    ------------------------------ */
    $('.counter-up').counterUp();

    /*------------------------------
    datepicker
    ------------------------------ */
    $("#datepicker").datepicker();

    /*---------------------
    smooth scroll
    --------------------- */
    $('.smoothscroll').on('click', function(e) {
        e.preventDefault();
        var target = this.hash;

        $('html, body').stop().animate({
            'scrollTop': $(target).offset().top - 80
        }, 1200);
    });

    /*---------------------
    countdown
    --------------------- */
    $('[data-countdown]').each(function() {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('<span class="cdown days"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hour</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>Min</p></span> <span class="cdown second"> <span><span class="time-count">%S</span> <p>Sec</p></span>'));
        });
    });

    /*---------------------
    video-popup
    --------------------- */
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 300,
        preloader: false,
        fixedContentPos: false
    });

    /*---------------------
    fancybox
    --------------------- */
    $('[data-fancybox]').fancybox({
        image: {
            protect: true
        }
    });

    /*---------------------
    match capcha
    --------------------- */
    $(function(){
    
        var mathenticate = {
            bounds: {
                lower: 5,
                upper: 50
            },
            first: 0,
            second: 0,
            generate: function()
            {
                this.first = Math.floor(Math.random() * this.bounds.lower) + 1;
                this.second = Math.floor(Math.random() * this.bounds.upper) + 1;
            },
            show: function()
            {
                return this.first + ' + ' + this.second;
            },
            solve: function()
            {
                return this.first + this.second;
            }
        };
        mathenticate.generate();
    
        var $auth = $('<input type="text" name="auth" />');
        $auth
            .attr('placeholder', mathenticate.show())
            .insertAfter('input[name="match_captcha"]');
        
        $('#cf').on('submit', function(e){
            e.preventDefault();
            if( $auth.val() != mathenticate.solve() )
            {
                alert('wrong answer!');
            }
        });
        
    });

    /*---------------------
    package-carousel
    --------------------- */
    function package_carousel() {
        var owl = $(".package-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 30,
            responsiveClass: true,
            navigation: true,
            navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],
            nav: true,
            items: 3,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                760: {
                    items: 2
                },
                994: {
                    items: 3
                }
            }
        });
    }
    package_carousel();

    /*---------------------
    testimonial-carousel
    --------------------- */
    function testimonial_carousel() {
        var owl = $(".testimonial-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 50,
            responsiveClass: true,
            navigation: true,
            navText: ["<i class='fas fa-long-arrow-alt-left'></i>", "<i class='fas fa-long-arrow-alt-right'></i>"],
            nav: true,
            items: 2,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                760: {
                    items: 1
                },
                994: {
                    items: 2
                }
            }
        });
    }
    testimonial_carousel();

    /*---------------------
    logo-carousel
    --------------------- */
    function logo_carousel() {
        var owl = $(".logo-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 15,
            responsiveClass: true,
            navigation: true,
            navText: ["<i class='fas fa-long-arrow-alt-left'></i>", "<i class='fas fa-long-arrow-alt-right'></i>"],
            nav: false,
            items: 5,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                760: {
                    items: 3
                },
                994: {
                    items: 5
                }
            }
        });
    }
    logo_carousel();

    /*---------------------
    v2-hero-carousel
    --------------------- */
    function v2_hero_carousel() {
        var owl = $(".v2-hero-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 15,
            responsiveClass: true,
            navigation: true,
            navText: ["<img src='../img/home2/left.png'>", "<img src='../img/home2/right.png'>"],
            nav: true,
            items: 1,
            smartSpeed: 2000,
            dots: true,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                }
            }
        });
    }
    v2_hero_carousel();

    /*---------------------
    v2-hotel-caraousel
    --------------------- */
    function v2_hotel_carousel() {
        var owl = $(".v2-hotel-caraousel");
        owl.owlCarousel({
            loop: true,
            margin: 15,
            responsiveClass: true,
            navigation: true,
            navText: ["<img src='../img/home2/left.png'>", "<img src='../img/home2/right.png'>"],
            nav: false,
            items: 1,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                }
            }
        });
    }
    v2_hotel_carousel();

    /*---------------------
    team-carousel
    --------------------- */
    function team_carousel() {
        var owl = $(".team-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 30,
            responsiveClass: true,
            navigation: true,
            navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
            nav: true,
            items: 4,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                760: {
                    items: 2
                },
                994: {
                    items: 3
                },
                1024: {
                    items: 4
                }
            }
        });
    }
    team_carousel();

    /*---------------------
    blog-carousel
    --------------------- */
    function blog_carousel() {
        var owl = $(".blog-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 0,
            responsiveClass: true,
            navigation: true,
            navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
            nav: true,
            items: 1,
            smartSpeed: 2000,
            dots: false,
            autoplay: true,
            autoplayTimeout: 4000,
            center: false,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                760: {
                    items: 1
                },
                994: {
                    items: 1
                },
                1024: {
                    items: 1
                }
            }
        });
    }
    blog_carousel();

    /*---------------------
    // Ajax Contact Form
    --------------------- */
    $('.cf-msg').hide();
    $('form#cf button#submit').on('click', function() {
        var fname = $('#fname').val();
        var pnumber = $('#pnumber').val();
        var email = $('#email').val();
        var msg = $('#msg').val();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!regex.test(email)) {
            alert('Please enter valid email');
            return false;
        }

        fname = $.trim(fname);
        pnumber = $.trim(pnumber);
        email = $.trim(email);
        msg = $.trim(msg);

        if (fname != '' && email != '' && msg != '') {
            var values = "fname=" + fname + "&pnumber=" + pnumber + "&email=" + email + " &msg=" + msg;
            $.ajax({
                type: "POST",
                url: "mail.php",
                data: values,
                success: function() {
                    $('#fname').val('');
                    $('#pnumber').val('');
                    $('#email').val('');
                    $('#msg').val('');

                    $('.cf-msg').fadeIn().html('<div class="alert alert-success"><strong>Success!</strong> Email has been sent successfully.</div>');
                    setTimeout(function() {
                        $('.cf-msg').fadeOut('slow');
                    }, 4000);
                }
            });
        } else {
            $('.cf-msg').fadeIn().html('<div class="alert alert-danger"><strong>Warning!</strong> Please fillup the informations correctly.</div>')
        }
        return false;
    });


}(jQuery));
