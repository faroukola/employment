<!DOCTYPE html>
<html lang="ar">

    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title>Home</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/stellarnav.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
         <link rel="shortcut icon" type="image/png" href="img/IMG_23052021_104240_(116_x_67_pixel).jpg">
     
    </head>

    <body style="height:850px">
        <div id="preloader"></div>
        <header id="_header">
            <div class="header-top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-8 col-12">
                            <div class="hta-contact">
                                <a href="tel:01069663900" class="text"><span class="icon"><i
                                            class="fas fa-phone"></i></span>(+880) 123456789</a>
                                <a href="mailto:elshadid_mohamed@yahoo.com" class="text"><span class="icon"><i
                                            class="fas fa-envelope"></i></span>elshadid_mohamed@yahoo.com</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-12">
                            <ul class="hta-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-5 col-sm-12 col-12">
                            <div class="hta-infos">
                                <a href="#" class="text"><span class="icon"><i class="fas fa-user"></i></span>My account</a>
                                <a href="#" class="text"><span class="icon"><i class="fas fa-heart"></i></span>Wishlist</a>                             
                                <a href="#" class="text"><span class="icon"><i class="fas fa-cart-plus"></i></span>Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-12">
                            <div class="logo">
                                <a href="index.php" class="link"><img src="img/IMG_23052021_104240_(116_x_67_pixel).jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-6 col-6">
                            <div class="main-menu stellarnav">
                                <ul>
                                    <li><a href="index.php">الرئيسية</a> </li>
                                    <li><a href="#">عرض الرحلات</a></li>
                                    <li><a href="login.php">تسجل الدخول</a></li>
                                    <li><a href="create_account.php">التسجيل في الموقع</a></li>
                                     <li><a href="Team.php">فريق العمل</a></li>
                                    <li><a href="Contact.php">اتصل بنا</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-2 col-6">
                            <div class="search">
                        <button type="button" class="search-btn btn" data-toggle="modal" data-target="#exampleModalLong"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="tour-package">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2 class="title">برامج سياحية</h2>
                        </div>
                    </div>
                </div>
                <div class="package-carousel owl-carousel">
                    <div class="single-package">
                        <div class="img">
                            <a href="#"><img src="img/home1/package-1.jpg" alt=""></a>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">طوكيو - ٤ أيام في كوريا ، إنترتيكا</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span>5 ايام</h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from">السعر</h5>
                                    <h3 class="price">$1500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-package">
                        <div class="img">
                            <a href="#"><img src="img/home1/package-2.jpg" alt=""></a>
                            <span class="off">15% خصم</span>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">جزر المالديف</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span>  ايام 3<span> ليالي 5</span></h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from cross">1500</h5>
                                    <h3 class="price">$3000</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-package">
                        <div class="img">
                            <a href="#"><img src="img/home1/package-3.jpg" alt=""></a>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">جزر الكناري</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span> 8 ساعات</h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from">السعر</h5>
                                    <h3 class="price">$1000</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-package">
                        <div class="img">
                            <a href="#"><img src="img/home1/package-1.jpg" alt=""></a>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">الصين</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span> 5 ايام</h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from">السعر</h5>
                                    <h3 class="price">$1500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-package">
                        <div class="img">
                            <a href="#"><img src="img/home1/package-2.jpg" alt=""></a>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">كندا</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span>  ايام 3<span> ليالي 5</span></h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from cross">1500</h5>
                                    <h3 class="price">$3000</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-package">
                        <div class="img">
                        <a href="#"><img src="img/home1/package-3.jpg" alt=""></a>
                        </div>
                        <div class="content">
                            <div class="name-price">
                                <div class="np-name">
                                    <a href="#" class="name">ايطاليا</a>
                                    <h5 class="duration"><span class="icon"><i class="far fa-clock"></i></span> 8 ساعات</h5>
                                </div>
                                <div class="np-price">
                                    <h5 class="from">السعر</h5>
                                    <h3 class="price">$1000</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        
        
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/stellarnav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>
        <script src="js/jquery.scrollUp.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/theme.js"></script>
    </body>
</html>
