<!DOCTYPE html>
<html>

    <head>
        <!--- Basic Page Needs  -->
        <meta charset="utf-8">
        <title>Home</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Meta  -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/stellarnav.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/create account_1.css">
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/png" href="img/IMG_23052021_104240_(116_x_67_pixel).jpg">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>

    <body>
        <div id="preloader"></div>
        <header id="_header">
            <div class="header-top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-8 col-12">
                            <div class="hta-contact">
                                <a href="tel:01069663900" class="text"><span class="icon"><i
                                            class="fas fa-phone"></i></span>(+880) 123456789</a>
                                <a href="mailto:elshadid_mohamed@yahoo.com" class="text"><span class="icon"><i
                                            class="fas fa-envelope"></i></span>elshadid_mohamed@yahoo.com</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4 col-12" style="margin-left: 30%"> 
                            <ul class="hta-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="header-bottom-area"  style="position: 0">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-12">
                            <div class="logo">
                                <a href="index.php" class="link"><img src="img/IMG_23052021_104240_(116_x_67_pixel).jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-6 col-6">
                            <div class="main-menu stellarnav">
                                <ul>
                                    <li><a href="index.php">الرئيسية</a> </li>
                                    <li><a href="Show.php">عرض الرحلات</a></li>
                                    <li><a href="login.php">تسجل الدخول</a></li>
                                    <li><a href="create_account.php">التسجيل في الموقع</a></li>
                                    <li><a href="Team.php">فريق العمل</a></li>
                                    <li><a href="Contact.php">اتصل بنا</a></li>
                                   
                                  
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-2 col-6">
                            <div class="search">
                                <button type="button" class="search-btn btn" data-toggle="modal"
                                    data-target="#exampleModalLong"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </header>
        
            <div class="contact" style="z-index:999">
        <div class="row">
            <div class="col-md-5">
              
            </div>
            <div class="col-md-5" >
                <div class="col-md-6">
                    <h2 class="text-left">انشاء حجز</h2>
                </div>
                <div class="col-md-6">
                   
                </div>
               <hr>
               <form action="Resave.php" method="POST">
                <div class="row">
                    <label class="label col-md-2 control-label"> نوع الرحلة</label>
                    <div class="col-md-10">
                        <select class="form-control" required="نوع الرحلة" name="FlightType">
                            <option style="display:none"></option>
                            <option>ذهاب فقط</option>
                            <option>ذهاب وعوده</option>
                            <option>وجهات متعددة</option>
                        </select>
                       
                    </div>
                </div>
                     <div class="row">
                    <label class="label col-md-2 control-label"> اسم المستخدم</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" required="" name="UserName" placeholder="اسم المستخدم">
                    </div>
                </div>
                <div class="row">
                    <label class="label col-md-2 control-label">البريد الالكتروني</label>
                    <div class="col-md-10">
                        <input type="email" class="form-control" required="" name="email" placeholder="البريد الالكتروني">
                    </div>
                </div>
                     <div class="row">
                    <label class="label col-md-2 control-label"> المغادرة من</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" required="" name="Leaving" placeholder="المغادرة من">
                    </div>
                </div>
                     <div class="row">
                    <label class="label col-md-2 control-label">الواجهه</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" required="" name="inerface" placeholder="الواجهه">
                    </div>
                </div>
                     <div class="row">
                    <label class="label col-md-2 control-label"> تاريخ المغادرة</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control" required="" name="DeDate" placeholder="تاريخ المغادرة">
                    </div>
                </div>
                     <div class="row">
                    <label class="label col-md-2 control-label">تاريخ العودة</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control" required="" name="ReDate" placeholder="تاريخ العودة">
                    </div>
                </div>
                <div class="row">
                    <label class="label col-md-2 control-label">عدد المسافرين</label>
                    <div class="col-md-10">
                        <input type="number" class="form-control" required="" name="Nu_Passengers" placeholder="عددالمسافرين">
                    </div>
                </div>
                <div class="row">
                    <label class="label col-md-2 control-label">الدرجة</label>
                    <div class="col-md-10">
                        
                       <select class="form-control" required="تحديد الدرجة" name="Degree">
                            <option style="display: none"></option>
                            <option>سياحية</option>
                            <option>رجال اعمال</option>
                            <option>درجة اولي</option>
                        </select>
                    </div>
                </div>
              
                <input type="submit" value="حجز" class="submit btn btn-info" name="submit" id="submit" />
                <a href="index.php"><div class="btn btn-warning">الغاء</div></a>
                 </form>
            </div>
            
        </div>

    </div>
        
        
        
          <script>
            function lettersOnly(input) {
                var regex = /[^0-9]/gi;
                input.value = input.value.replace(regex, "");
            }
        </script>
        <!-- restricated-input-text -->
        <!-- Scripts -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/stellarnav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>
        <script src="js/jquery.scrollUp.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/theme.js"></script>
    </body>

</html>